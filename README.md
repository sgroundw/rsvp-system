# RSVP System Cookbook

RSVP System is a recipe project for building calendar application sites with [Drupal](https://drupal.org). 

## Overview

This project provides a flexible set of building blocks (recipes) for creating Room Reservation, Inventory Loan, Staff Schedule, and other date based applications. 

## Documentation

The /pubic folder within this repository serves as the documenation homepage for [RSVP-System.org](https://rsvp-system.org)**.


## The In-House Radix RSVP Theme is maintained here:
[RSVP-System In-House RSVP Radix Theme]
https://github.com/slackstone/radix_house_theme

RSVP System is open-source software licensed under the [GPL 2.0 License](https://gitlab.com/rsvp-system/rsvp-system/-/blob/main/LICENSE.md?ref_type=heads).

![Community_Taxonomy](https://gitlab.com/rsvp-system/rsvp-system/-/raw/main/data/screenshots/Community_Taxonomy.png)

