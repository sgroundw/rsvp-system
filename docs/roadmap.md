# RSVP System Roadmap

This document outlines the planned features and implementations for the RSVP System.

## Feature Highlights 
- Drupal Recipe Feature: Targets Drupal's new "Recipe" feature to provide easy feature deployment.
- RSVP Organizational Units: Taxonomy for Departments/Communities.
  - RSVP management contained within each Org Unit (Taxonomy Permission by Role).
  - Access Control by Org Unit (uses Drupal Roles and Taxonomy Terms).

- Content Types:
  - RSVP Resources: Room Locations, Equipment Inventory, Library Items, etc.
  - RSVP Request: Trimmed to available resources, by Org Unit Role.
- Moderation Workflows: For department teams.
- Flexible Workflow Models: Via ECA module (Events Conditions Actions).
- Reservation Reporting and Dashboard Blocks: Implemented with Views.
- Conflict Date Validation: To prevent overbooking and overlap.
- Recurring Dates:
  - Single Item Reservation (simple).
  - Dates Series - Multiple Item Series (complex, flexible).
- Lead Time Request Padding: Policy settings per resource.
    ECA workflow validation recipe for Lean Time (* Requires RSVP Location lead_time field value.)
- CSV Feed Imports:
  - For RSVP Resources (Content Type)
  - For RSVP Locations (COntent Type)
  - For RSVP Departments (Taxonomy)
  - Sample Data: Built around 10 Philadelphia neighborhoods.

## Atomic URL Patterns

The RSVP System will implement a standardized URL pattern to streamline access to various resources and locations.

{rsvp_department_short_name}/[location]|[resource]/[location_shortname]|[resource_shortname]


### Examples

- **Technology Resource**: `/technology/resource/laptop_1`
- **Arts League Location**: `/arts-league/location/clay_studio`

## Future Implementations

Additional features and improvements planned for the RSVP System will be documented here as the project progresses.

RSVP System Department Taxonomy (rsvp_system_departments)
Department Logo	(field_rsvp_system_department_logo)	
Entity reference Reference type: Media
Media type: Image

Department Short Name	(field_rsvp_system_department_short_name)
Text plain
